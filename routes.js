var express = require('express'),
	router = express.Router(),
	cors = require('cors'),
	request = require('request'),
	path = require('path'),
	controllers = require('./controllers/index.js');

module.exports = function(app) {
	router.all('*', cors({
		"optionsSuccessStatus": 200
	}));
	router.get('/', (req, res) => {
		res.sendFile(path.join(__dirname + "/client/build/index.html"));
	});
	//airport routes
	router.post('/api/airports/search', controllers.airport.search);
	router.post('/api/airport', controllers.airport.getSingle);
	//yelp routes
	router.post('/api/yelp/fetch', controllers.yelp.fetch);
	router.post('/api/yelp/single', controllers.yelp.single);
	//review routes
	router.post('/api/review/create', controllers.review.create);
	app.use('/', router);
}