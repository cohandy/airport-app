var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var AirportSchema = new Schema({
	name: { type: String },
	latitude: { type: Number },
	longitude: { type: Number },
	city: { type: String },
	country: { type: String },
	code: { type: String }
});
AirportSchema.index({name: 'text', city: 'text'});
module.exports = mongoose.model('airport', AirportSchema);