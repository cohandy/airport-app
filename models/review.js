var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var ReviewSchema = new Schema({
	name: { type: String, maxlength: [20, "Name cannot be longer than 20 characters"] },
	rating: { type: Number, required: [true, "Rating is required"] },
	body: { type: String, required: [true, "Review is required"] },
	placeId: { type: String }
});
module.exports = mongoose.model('review', ReviewSchema);