export function getPlaces(lat, long, offset=0, sort="rating") {
	return function(dispatch) {
		if(offset === 0) {
			dispatch({type: "FETCH_YELP_PLACES_START"});
		} else {
			dispatch({type: "FETCH_MORE_YELP_PLACES_START"});
		}

		fetch('/api/yelp/fetch', {
			method: "POST",
			body: `lat=${lat}&long=${long}&offset=${offset}&sort=${sort}`,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((json) => {
			if(json.success === "true") {
				if(offset === 0) {
					dispatch({type: "FETCH_YELP_PLACES_END", payload: json.places});
				} else {
					dispatch({type: "FETCH_MORE_YELP_PLACES_END", payload: json.places});
				}
			} else {
				throw json;
			}
		})
		.catch((error) => {
			dispatch({type: "FETCH_YELP_PLACES_ERROR"});
		});
	}
}

export function getSingle(id, place=null) {
	return function(dispatch) {
		if(place) {
			dispatch({type: "FETCH_SINGLE_YELP_PLACE_END", payload: place });
		} else {
			dispatch({type: "FETCH_SINGLE_YELP_PLACE_START"})
			fetch('/api/yelp/single', {
				method: "POST",
				body: `id=${id}`,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				credentials: 'same-origin'
			})
			.then((resp) => {
				if(resp.status === 200) {
					return resp.json();
				} else {
					throw resp.status;
				}
			})
			.then((json) => {
				if(json.success === "true" && json.place) {
					dispatch({type: "FETCH_SINGLE_YELP_PLACE_END", payload: json.place});
				} else {
					throw json;
				}
			})
			.catch((error) => {
				dispatch({type: "FETCH_SINGLE_YELP_PLACE_ERROR"});
			});
		}
	}
}