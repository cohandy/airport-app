export function search(q, offset) {
	return function(dispatch) {
		dispatch({type: "FETCH_AIRPORTS_START"});
		fetch('/api/airports/search', {
			method: "POST",
			body: `q=${q}&offset=${offset}`,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((json) => {
			if(json.success === "true") {
				if(offset === 0) {
					dispatch({type: "FETCH_AIRPORTS_END", payload: json.airports});
				} else {
					dispatch({type: "FETCH_MORE_AIRPORTS_END", payload: json.airports});
				}
			} else {
				throw "error";
			}
		})
		.catch((error) => {
			dispatch({type: "FETCH_AIRPORTS_ERROR"});
		});
	}
}

export function getSingle(code, airport=null) {
	return function(dispatch) {
		if(airport) {
			dispatch({type: "FETCH_SINGLE_AIRPORT_END", payload: airport});
		} else {
			dispatch({type: "FETCH_SINGLE_AIRPORT_START"});
			fetch('/api/airport', {
				method: "POST",
				body: `code=${code}`,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				credentials: 'same-origin'
			})
			.then((resp) => {
				if(resp.status === 200) {
					return resp.json();
				} else {
					throw resp.status;
				}
			})
			.then((json) => {
				if(json.success === "true" && json.airport) {
					dispatch({type: "FETCH_SINGLE_AIRPORT_END", payload: json.airport});
				} else {
					throw "error";
				}
			})
			.catch((error) => {
				dispatch({type: "FETCH_SINGLE_AIRPORT_ERROR"});
			});
		}
	}
}