export function create(formObj) {
	return function(dispatch) {
		dispatch({type: "CREATE_REVIEW_START"});
		fetch('/api/airports/search', {
			method: "POST",
			body: `name=${formObj.name}&rating=${formObj.rating}&body=${formObj.body}`,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((json) => {
			if(json.success === "true") {
				if(json.hasOwnProperty('review')) {
					dispatch({type: "CREATE_REVIEW_END", payload: json.review});
				} else {
					console.log(json);
					dispatch({type: "CREATE_REVIEW_ERROR", payload: "Error"})
				}
			} else {
				throw "error";
			}
		})
		.catch((error) => {
			dispatch({type: "CREATE_REVIEW_ERROR", payload: "Database error, please try again later"});
		});
	}
}