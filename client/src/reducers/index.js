import { combineReducers } from "redux";

import airport from "./airportReducer";
import yelp from "./yelpReducer";

export default combineReducers({
	airport,
	yelp
});