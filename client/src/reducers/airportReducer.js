const defaultState = {
	search: {
		airports: [],
		loading: false,
		error: false,
		foundAll: false
	},
	airportPage: {
		currentAirport: null,
		error: false,
		loading:false
	}
}

export default function airportReducer(state=defaultState, action) {
	switch(action.type) {
		//search actions
		case "FETCH_AIRPORTS_START": {
			state = {...state, search: {...state.search, loading: true, error: false} }
			break;
		}
		case "FETCH_AIRPORTS_END": {
			if(action.payload.length >= 15) {
				state = {...state, search: {airports: action.payload, loading: false, error: false, foundAll: false} }
			} else {
				state = {...state, search: {airports: action.payload, loading: false, error: false, foundAll: true} }
			}
			break;
		}
		case "FETCH_MORE_AIRPORTS_END": {
			if(action.payload.length >= 15) {
				state = {...state, search: {...state.search, airports: state.search.airports.concat(action.payload), loading: false, error: false, foundAll: false} }
			} else {
				state = {...state, search: {...state.search, airports: state.search.airports.concat(action.payload), loading: false, error: false, foundAll: true} }
			}
			break;
		}
		case "FETCH_AIRPORTS_ERROR": {
			console.log("error fetch airports");
			state = {...state, search: {airports: [], loading: false, error: true, foundAll: false} }
			break;
		}
		//single airport fetch actions
		case "FETCH_SINGLE_AIRPORT_END": {
			state = {...state, airportPage: {...state.airportPage, currentAirport: action.payload, error: false, loading:false} }
			break;
		}
		case "FETCH_SINGLE_AIRPORT_START": {
			state = {...state, airportPage: { ...state.airportPage, currentAirport: null, error:false, loading:true} }
		}
		case "FETCH_SINGLE_AIRPORT_ERROR": {
			state = {...state, airportPage: { ...state.airportPage, currentAirport: null, error: true} }
			break;
		}
		default: {
			return state;
		}
	}
	return state;
}