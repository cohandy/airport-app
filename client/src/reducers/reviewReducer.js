const defaultState = {
	create: {
		error:false,
		errorMessage:null,
		loading:false,
		review:null
	},
	placePage: {
		reviews: []
	}
}

export default function reviewReducer(state=defaultState, action) {
	switch(action.type) {
		case "CREATE_REVIEW_START": {
			state = { ...state, create: { ...state, loading:true, error:false, review:null } }
			break;
		}
		case "CREATE_REVIEW_ERROR": {
			state = { ...state, create: { ...state, loading:false, error:true, review:null, errorMessage:action.payload } }
			break;
		}
		case "CREATE_REVIEW_END": {
			state = { ...state, create: { ...state, loading:false, error:false, review:action.payload } }
			break;
		}
		default: {
			return state;
		}
	}
}