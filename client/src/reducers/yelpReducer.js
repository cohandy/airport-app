const defaultState = {
	airportPage: {
		places: [],
		error: false,
		loading: false,
		foundAll: false
	},
	placePage: {
		currentPlace: null,
		error: false,
		loading: false
	}
}

export default function reducer(state=defaultState, action) {
	switch(action.type) {
		case "FETCH_YELP_PLACES_START": {
			state = {...state, airportPage: {...state.airportPage, places: [], loading:true, error:false} }
			break;
		}
		case "FETCH_MORE_YELP_PLACES_START": {
			state = {...state, airportPage: {...state.airportPage, loading:true, error:false} }
			break;
		}
		case "FETCH_YELP_PLACES_END": {
			if(action.payload.length >= 15) {
				state = {...state, airportPage: {...state.airportPage, loading:false, error:false, places: action.payload, foundAll: false} }
			} else {
				state = {...state, airportPage: {...state.airportPage, loading:false, error:false, places: action.payload, foundAll:true } }
			}
			break;
		}
		case "FETCH_MORE_YELP_PLACES_END": {
			if(action.payload.length >= 15) {
				state = {...state, airportPage: {...state.airportPage, loading:false, error:false, places: state.airportPage.places.concat(action.payload), foundAll: false} }
			} else {
				state = {...state, airportPage: {...state.airportPage, loading:false, error:false, places: state.airportPage.places.concat(action.payload), foundAll:true } }
			}
			break;
		}
		case "FETCH_YELP_PLACES_ERROR": {
			state = {...state, airportPage: {...state.airportPage, places:[], loading:false, error:true} }
			break;
		}
		case "FETCH_SINGLE_YELP_PLACE_START": {
			state = {...state, placePage: {...state.placePage, error:false, loading:true} }
			break;
		}
		case "FETCH_SINGLE_YELP_PLACE_END": {
			state = {...state, placePage: {...state.placePage, currentPlace: action.payload, error: false, loading:false} }
			break;
		}
		case "FETCH_SINGLE_YELP_PLACE_ERROR": {
			state = {...state, placePage: {...state.placePage, currentPlace: null, error: true, loading:false} }
			break;
		}
		default: {
			return state;
		}
	}
	return state;
}