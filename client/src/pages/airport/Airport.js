import React from 'react';
import Media from 'react-media';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter, Route, Link } from 'react-router-dom';

import Place from '../place/Place';
import Places from './Places';
import PlaceSort from './PlaceSort';

import * as yelpActions from '../../actions/yelpActions';
import * as airportActions from '../../actions/airportActions';

import AppLoading from '../AppLoading';

class Airport extends React.Component {

	constructor(props) {
		super(props);
		this.initialFetch = false;
		this.state = {
			currentSort: 'rating'
		}
	}

	componentWillMount() {
		this.findAirport();
	}

	componentWillReceiveProps(nextProps) {
		if(this.props.match.params.id !== nextProps.match.params.id) {
			this.findPlace(nextProps);
		}
	}

	componentDidUpdate() {
		var currentAirport = this.props.airport.airportPage.currentAirport;
		if(currentAirport && !this.initialFetch) {
			this.props.yelpActions.getPlaces(currentAirport.latitude, currentAirport.longitude);
			this.initialFetch = true;
		}
	}

	findAirport(nextProps=null) {
		var airportProps = this.props;
		if(nextProps) airportProps = this.props;
		//check if new page needs to fetch airport
		var currentAirport = airportProps.airport.airportPage.currentAirport;
		if(!currentAirport || currentAirport.code.toLowerCase() !== airportProps.match.params.code) {
			this.initialFetch = false;
			//if there are results from the search, loop through search and find airport picked, skip fetch request
			var searchAirports = airportProps.airport.search.airports;
			if(searchAirports.length > 0) {
				for(var i=0;i<searchAirports.length;i++) {
					if(searchAirports[i].code.toLowerCase() === airportProps.match.params.code) {
						airportProps.airportActions.getSingle("X", searchAirports[i]);
						break;
					}
				}
			} else {
				airportProps.airportActions.getSingle(airportProps.match.params.code);
			}
		}
	}

	loadMorePlaces(type) {
		if(type === "yelp") {
			var offset = this.props.yelp.airportPage.places.length - 1,
				currentAirport = this.props.airport.airportPage.currentAirport;
			this.props.yelpActions.getPlaces(currentAirport.latitude, currentAirport.longitude, offset);
		}
	}

	changeSort(option) {
		var currentAirport = this.props.airport.airportPage.currentAirport;
		this.props.yelpActions.getPlaces(currentAirport.latitude, currentAirport.longitude, 0, option);
		this.setState({
			currentSort: option
		});
	}

	render() {
		var currentAirport = this.props.airport.airportPage.currentAirport;

		//if active currentAirport
		if(currentAirport && currentAirport.code.toLowerCase() === this.props.match.params.code) {
			return (
				<div className="interior-container">
					<section id="airports-section">
						<PlaceSort airportName={this.props.airport.airportPage.currentAirport.name} currentSort={this.state.currentSort} changeSort={this.changeSort.bind(this)}/>
						{/* Yelp Places */}
						<Places url={this.props.match.url} from="Yelp" page={this.props.yelp.airportPage} loadMore={this.loadMorePlaces.bind(this)}/>
					</section>
					{/* if screen is at least 1201px wide, render place component in airport page */}
					<Media query="(min-width:1201px)" render={() => (
						<Route path="/airport/:code/:id" component={Place}/>
					)}/>
					<div id="fixed-divider"></div>
				</div>
			)
		//if error occurred in fetch request
		} else if(this.props.airport.airportPage.error) {
			return (
				<div className="interior-container">
					<h2>Error occured, please try reloading the page</h2>
				</div>
			)
		//if loading
		} else {
			return (
				<AppLoading/>
			)
		}
	}
}

function mapStateToProps(state) {
	return {
		user: state.user,
		yelp: state.yelp,
		airport: state.airport
	}
}

function mapDispatchToProps(dispatch) {
	return {
		airportActions: bindActionCreators(airportActions, dispatch),
		yelpActions: bindActionCreators(yelpActions, dispatch)
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Airport));