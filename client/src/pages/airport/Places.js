import React from 'react';

import PlaceLink from './PlaceLink';
import gif from '../../images/loading-bars.svg';

const Places = (props) => {

	if(props.page.places.length > 0) {
		places = props.page.places.map((place, index) => {
			return <PlaceLink place={place} url={props.url} key={index}/>
		});
	}

	var places = props.page.places.map((place, index) => {
		return <PlaceLink place={place} url={props.url} key={index}/>
	});
	if(props.page.places.length === 0 && !props.page.loading && !props.page.error) {
		places = <div className="fetch-error">No Restaurants found</div>
	}

	//if loading
	var loading = '';
	if(props.page.loading) {
		loading = <img src={gif} className="loading-gif"/>;
	}
	
	var loadMore = '';
	if(!props.page.foundAll && !props.page.loading) {
		loadMore = (
			<div className="button-container">
				<div onClick={ () => props.loadMore("yelp") } className="theme-button">LOAD MORE</div>
			</div>
		)
	}
	var error = '';
	if(props.page.error) {
		error = <div className="fetch-error">An error has occurred, please reload the page!</div>;
	}

	return (
		<div id="yelp-places">
			<div className="place-type-divider">Results from {props.from}</div>
			{places}
			{loadMore}
			{loading}
			{error}
		</div>
	)
}

export default Places;