import React from 'react';

const PlaceSort = (props) => {

	var options = ['rating', 'best_match', 'reviews'];

	var optionBlocks = options.map((option, index) => {
		var classes = `option ${option}`;
		if(option === props.currentSort) {
			classes += " active";
		}
		return (
			<div className={classes} key={index} onClick={() => props.changeSort(option) }>
				<span>{option.split('_').join(' ').toUpperCase()}</span>
			</div>
		)
	});

	return (
		<div id="place-sort">
			<h3>{props.airportName}</h3>
			<div className="sort-options">
				{optionBlocks}
			</div>
		</div>
	)
}

export default PlaceSort;