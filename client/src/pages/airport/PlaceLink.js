import React from 'react';
import { Link } from 'react-router-dom';

import placeDefaultPic from '../../images/place-default.png';

const PlaceLink = (props) => {

	//if place has a url prop, then its a place from yelp
	var categories = [],
		linkTo = props.url + '/' + props.place.id
	if(props.place.hasOwnProperty('url')) {
		var	yelpCats = props.place.categories;
		if(yelpCats.length > 0) {
			for(var i=0;i<yelpCats.length;i++) {
				if(i > 2) break;
				categories.push(yelpCats[i].title);
			}
		}
	}

	var image = placeDefaultPic;
	if(props.place.hasOwnProperty('image_url')) {
		image = props.place.image_url;
	}	

	return (
		<Link to={linkTo} className="place-link">
			<div className="top">
				<div className="pic">
					<img src={image}/>
				</div>
				<div className="title-info">
					<h2>{props.place.name}</h2>
					<p>{ categories.join('    /    ') }</p>
				</div>
			</div>
			<div className="bottom">
				<div className="info-block-container">
					<div className="info-block">
						<span>RATING</span>
						<div>{props.place.rating}/5</div>
					</div>
					<div className="info-block">
						<span>PRICE</span>
						<div>{props.place.price}</div>
					</div>
					<div className="info-block">
						<span>REVIEWS</span>
						<div className="last-block">{props.place.review_count}</div>
					</div>
				</div>
			</div>
		</Link>
	)
}

export default PlaceLink;