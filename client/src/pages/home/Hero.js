import React from 'react';
import { Link } from 'react-router-dom';

export default class Hero extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			searchActive: false
		}
	}

	getAirports() {
		this.props.fetch(this._input.value, 0);
	}

	searchScroll() {
		var divHeight = this._results.clientHeight,
			scrollTop = this._results.scrollTop;

		if(scrollTop + 50 >= divHeight && !this.props.search.foundAll) {
			var currentAirports = document.querySelectorAll('.search-results .result');
			this.props.fetch(this._input.value, currentAirports.length);
		}
	}

	render() {
		var airports = this.props.search.airports,
			resultsStyle = this.state.searchActive ? {display: 'block'} : {},
			activeInputStyles = this.state.searchActive && airports.length;

		const airportComponents = airports.map((airport, index) => {
			return <AirportResult airport={airport} key={`airport_${index}`}/>
		});

		var searchError = "";
		if(this.props.search.error) {
			searchError = <div className="result error">
							<span>An error has occurred! Please reload the page and try again.</span>
						</div>
		}

		return (
			<section id="home-hero">
				<div className="hero-content">
					<div className="custom-container">
						<h2>FAMISHED FLYER</h2>
						<p>Now boarding at a restaurant near you</p>
						<div className="search-bar">
							<div className="input-submit-combine">
								<input type="text" name="q" placeholder="Which airport are you at?" autoComplete="off"
								       onFocus={() => this.setState({ searchActive: true })}
									   onKeyUp={this.getAirports.bind(this)} 
									   ref={(el) => this._input = el } 
									   style={ activeInputStyles ? {borderRadius: '2.5px 0 0 0', borderBottom: '1px solid #e6e6e6'} : {} }/>
								<input type="submit" className="theme-button" value="Search" style={ activeInputStyles ? {borderRadius: '0 2.5px 0 0', borderBottom: '1px solid #e6e6e6'} : {} }/>
							</div>
							<div className="search-results" ref={ (el) => this._results = el } onScroll={this.searchScroll.bind(this)} style={resultsStyle}>
								{searchError}
								{airportComponents}
							</div>
						</div>
					</div>
				</div>
			</section>
		)
	}
}

const AirportResult = (props) => {
	const airportHandle = props.airport.name.split(" ").join('-').toLowerCase();
	return (
		<Link to={`/airport/${props.airport.code.toLowerCase()}`} className="result">
			<span>{ props.airport.name }</span>
		</Link>
	)
}