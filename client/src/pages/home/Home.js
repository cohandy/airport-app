import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as airportActions from '../../actions/airportActions';

import Hero from './Hero';

class Home extends React.Component {

	componentWillMount() {
		
	}

	render() {
		return (
			<div>
				<Hero fetch={this.props.airportActions.search} search={this.props.airport.search}/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		user: state.user,
		airport: state.airport
	}
}

function mapDispatchToProps(dispatch) {
	return {
		airportActions: bindActionCreators(airportActions, dispatch)
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));