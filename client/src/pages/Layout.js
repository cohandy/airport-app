import React from 'react';
import Media from 'react-media';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Link, Redirect, withRouter, Switch } from 'react-router-dom';
import Home from './home/Home';
import Airport from './airport/Airport';
import Place from './place/Place';

import 'normalize.css';
import '../styles/index.css';

export default class Layout extends React.Component {

	render() {
		return (
			<Router>
				<main>
					<Media query={ { maxWidth:1200 } }>
						{matches => matches
						?
							<Switch>
								<Route exact path="/" component={Home}/>
								<Route path="/airport/:code/:id" component={Place}/>
								<Route path="/airport/:code" component={Airport}/>
							</Switch>
						:
							<Switch>
								<Route exact path="/" component={Home}/>
								<Route path="/airport/:code" component={Airport}/>
							</Switch>
						}
					</Media>
				</main>
			</Router>
		)
	}
}

const Navbar = (props) => {
	var navStyle = {}
	return (
		<nav id="navbar" style={navStyle}>
			<div className="custom-container">
				<div>
					<a href="#">Write a Review</a>
				</div>
				<div className="right">
					<a href="#">Login</a>
					<span>&nbsp;/&nbsp;</span>
					<a href="#">Sign Up</a>
				</div>
			</div>
		</nav>
	)
}