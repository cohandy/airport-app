import React from 'react';

import loadingSvg from '../images/loading-bars-gray.svg';

export default class AppLoading extends React.Component {
	render() {
		return (
			<div id="app-loading-overlay">
				<img src={loadingSvg} className="app-loading-svg"/>
			</div>
		)
	}
}