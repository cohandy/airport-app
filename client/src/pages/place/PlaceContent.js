import React from 'react';

import WriteReview from './WriteReview';

export default class PlaceContent extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {

		var headerStyle = {}
		if(this.props.place.hasOwnProperty('image_url')) {
			headerStyle = {
				background: `url(${this.props.place.image_url}) no-repeat center`
			}
		}

		var categories = [];
		if(this.props.place.hasOwnProperty('url')) {
			var	yelpCats = this.props.place.categories;
			if(yelpCats.length > 0) {
				for(var i=0;i<yelpCats.length;i++) {
					if(i > 2) break;
					categories.push(yelpCats[i].title);
				}
			}
		}

		return (
			<div id="place-content">
				<div className="place-header" style={headerStyle}>
					<div className="content">
						<h2>{this.props.place.name}</h2>
						<p>{ categories.join('    /    ') }</p>
					</div>
					<div className="bottom">
						<span>{this.props.place.rating}/5</span>
						<span className="price">{this.props.place.price}</span>
					</div>
				</div>
				{/* <WriteReview reviewActions={this.props.reviewActions}/> */}
				<div className="place-info">
					<div className="info-div">
						<span>Address:</span>
						<p>{this.props.place.location.address1}</p>
					</div>
					<div className="info-div">
						<span>City:</span>
						<p>{this.props.place.location.city}</p>
					</div>
					<div className="info-div">
						<span>Phone:</span>
						<p>{this.props.place.display_phone}</p>
					</div>
					<div className="info-div">
						<a href={this.props.place.url} target="_blank">Find out more on yelp!</a>
					</div>
				</div>
			</div>
		)
	}
}