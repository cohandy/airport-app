import React from 'react';

export default class WriteReview extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			formActive: false,
			name: '',
			rating: '',
			body: ''
		}
	}

	activateForm() {
		var active = true;
		if(this.state.formActive) {
			active = false;
		}
		this.setState({
			formActive: active
		});
	}

	handleInputChange(e) {
		const name = e.target.name;
		this.setState({
			[name]: e.target.value
		});
	}

	createReview(e) {
		e.preventDefault();
		this.props.reviewActions.create(this.state);
	}

	render() {

		var formStyle = {}
		if(this.state.formActive) {
			formStyle = {
				height: '370px'
			}
		}

		return (
			<div>
				<div className="write-review-button" onClick={() => this.activateForm() }>
					<h3>WRITE A REVIEW</h3>
				</div>
				<form id="review-form" style={formStyle} onSubmit={this.createReview.bind(this)}>
					<div className="form-input">
						<input type="text" name="name" value={this.state.name} onChange={this.handleInputChange.bind(this)} placeholder="Name"/>
					</div>
					<div className="form-input">
						<input type="number" name="rating" value={this.state.rating} onChange={this.handleInputChange.bind(this)} placeholder="Rating"/>
					</div>
					<div className="form-input">
						<textarea placeholder="Description" name="body" value={this.state.body} onChange={this.handleInputChange.bind(this)}/>
					</div>
					<div className="form-input">
						<input type="submit" value="SUBMIT" className="theme-button"/>
					</div>
				</form>
			</div>
		)
	}
}