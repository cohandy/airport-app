import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter, Route } from 'react-router-dom';

import * as yelpActions from '../../actions/yelpActions';
import * as airportActions from '../../actions/airportActions';
import * as reviewActions from '../../actions/reviewActions';

import PlaceContent from './PlaceContent';
import AppLoading from '../AppLoading';

class Place extends React.Component {

	constructor(props) {
		super(props);
		this.initialFetch = false;
	}

	componentWillMount() {
		this.findPlace();
	}

	componentWillReceiveProps(nextProps) {
		if(this.props.match.params.id !== nextProps.match.params.id) {
			this.findPlace(nextProps);
		}
	}

	componentDidUpdate() {
		if(this.props.yelp.placePage.currentPlace && !this.initialFetch) {
			//this.props.yelpActions.getPlaces(currentAirport.latitude, currentAirport.longitude);
			this.initialFetch = true;
		}
	}

	findPlace(nextProps=null) {
		var placeProps = this.props;
		if(nextProps) placeProps = nextProps;
		//if yelp is at the end of id in url then place is from yelp
		if(!placeProps.yelp.placePage.currentPlace || placeProps.yelp.placePage.currentPlace.id !== placeProps.match.params.id) {
			this.initialFetch = false;
			var airportPlaces = placeProps.yelp.airportPage.places;
			if(airportPlaces.length > 0) {
				for(var i=0;i<airportPlaces.length;i++) {
					if(airportPlaces[i].id === placeProps.match.params.id) {
						placeProps.yelpActions.getSingle(placeProps.match.params.id, airportPlaces[i]);
						break;
					}
				}
			} else {
				placeProps.yelpActions.getSingle(placeProps.match.params.id);
			}
		}
	}

	render() {
		if(this.props.yelp.placePage.currentPlace && this.props.yelp.placePage.currentPlace.id === this.props.match.params.id) {
			return (
				<section id="place-section">
					<div className="interior-container">
						<PlaceContent place={this.props.yelp.placePage.currentPlace} reviewActions={this.props.reviewActions}/>
					</div>
				</section>
			)
		} else if(this.props.yelp.placePage.error) {
			return (
				<section id="place-section">
					<div className="interior-container">
						<h2>An error has occurred, please refresh the page</h2>
					</div>
				</section>
			)
		} else {
			return (
				<AppLoading/>
			)
		}
	}
}

function mapStateToProps(state) {
	return {
		yelp: state.yelp,
		airport: state.airport
	}
}

function mapDispatchToProps(dispatch) {
	return {
		airportActions: bindActionCreators(airportActions, dispatch),
		yelpActions: bindActionCreators(yelpActions, dispatch),
		reviewActions: bindActionCreators(reviewActions, dispatch)
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Place));