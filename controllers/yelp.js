const request = require('request');
const async = require('async');

module.exports = {
	fetch: function(req, res, next) {
		var places = [];
		async.series([
			function(callback) {
				if(!req.session.token) {
					var clientId = "KuXJm4NXC62LYUsOUctiLw",
						clientSecret = "AeS4HNbXcw0KabEIJmFRcZNH92FJR0VRC5vyipvSv0xvy9CJR6UODpau0pgvFM3p";

					var options = {
						url: 'https://api.yelp.com/oauth2/token',
						headers: {
							'content-type': 'application/x-www-form-urlencoded'
						},
						body: 'grant_type=client_credentials&client_id=' + clientId + '&client_secret=' + clientSecret,
						method: 'POST'
					}
					request(options, function(err, resp, body) {
						if(resp.statusCode == 200) {
							var jsonResp = JSON.parse(body);
							req.session.token = jsonResp.access_token;
							req.session.save();
							callback();
						} else {
							callback('false');
						}
					});
				} else {
					callback();
				}
			},
			function(callback) {
				var url = "https://api.yelp.com/v3/businesses/search?",
					sortBy = req.body.sort;
					offset = req.body.offset || 0;
				if(sortBy === "reviews") {
					sortBy = "review_count";
				}
				url += `latitude=${req.body.lat}&longitude=${req.body.long}&radius=3500&term=food&limit=15&offset=${offset}&sort_by=${sortBy}`;
				var options = {
					url: url,
					headers: {
						'Authorization': "Bearer " + req.session.token
					},
					method: 'GET'
				}
				request(options, function(err, resp, body) {
					if(resp.statusCode === 200) {
						var json = JSON.parse(body);
						places = json.businesses;
						callback();
					} else {
						callback('false');
					}
				});
			}
		], function(err) {
			if(err) res.json({'success': 'false'});
			res.json({'success': 'true', 'places': places});
		});
	},
	single: function(req, res, next) {
		var place = null;
		async.series([
			function(callback) {
				if(!req.session.token) {
					var clientId = "KuXJm4NXC62LYUsOUctiLw",
						clientSecret = "AeS4HNbXcw0KabEIJmFRcZNH92FJR0VRC5vyipvSv0xvy9CJR6UODpau0pgvFM3p";

					var options = {
						url: 'https://api.yelp.com/oauth2/token',
						headers: {
							'content-type': 'application/x-www-form-urlencoded'
						},
						body: 'grant_type=client_credentials&client_id=' + clientId + '&client_secret=' + clientSecret,
						method: 'POST'
					}
					request(options, function(err, resp, body) {
						if(resp.statusCode == 200) {
							var jsonResp = JSON.parse(body);
							req.session.token = jsonResp.access_token;
							req.session.save();
							callback();
						} else {
							callback('false');
						}
					});
				} else {
					callback();
				}
			},
			function(callback) {
				var url = `https://api.yelp.com/v3/businesses/${req.body.id}`;
				var options = {
					url: url,
					headers: {
						'Authorization': "Bearer " + req.session.token
					},
					method: 'GET'
				}
				request(options, function(err, resp, body) {
					if(resp.statusCode === 200) {
						var json = JSON.parse(body);
						place = json;
						callback();
					} else {
						callback('false');
					}
				});
			}
		], function(err) {
			if(err) res.json({'success': 'false'});
			res.json({'success': 'true', 'place': place});
		});
	}
}