module.exports = {
	airport: require('./airport'),
	yelp: require('./yelp'),
	review: require('./review')
}