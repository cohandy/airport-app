var AirportModel = require('../models/airport');

module.exports = {
	search: function(req, res, next) {
		var offset = parseInt(req.body.offset) || 0;
		AirportModel.find({$text: { $search: req.body.q } }).skip(offset).limit(15).exec(function(err, airports) {
			if(err) return next(err);
			res.json({'success': 'true', 'airports': airports});
		});
	},
	getSingle: function(req, res, next) {
		AirportModel.findOne({code: req.body.code.toUpperCase()}).exec(function(err, airport) {
			if(err) return next(err);
			res.json({'success': 'true', 'airport': airport});
		});
	}
}