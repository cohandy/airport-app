var ReviewModel = require('../models/review');

module.exports = {
	create: function(req, res, next) {
		var newReview = new ReviewModel({
			name: req.body.name,
			body: req.body.body,
			rating: req.body.rating,
			placeId: req.body.placeId
		});
		newReview.submit(function(err, review) {
			if(err) {
				err.success = true;
				res.json(err);
			} else {
				res.json({'success': true, 'review': review});
			}
		});
	},
	fetch: function(req, res, next) {
		var offset = req.body.offset || 0;
		ReviewModel.find({placeId: req.body.placeId}).offset(offset).limit(10).exec(function(err, reviews) {
			if(err) return next(err);
			res.json({'success': 'true', reviews: reviews});
		});
	}
}